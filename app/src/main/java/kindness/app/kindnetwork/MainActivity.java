package kindness.app.kindnetwork;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import Model.ActivityStack;
import Model.GpsTracker;
import Model.Utils;

public class MainActivity extends AppCompatActivity {
    LinearLayout ll_login, ll_signup, ll_facebook;
    Menu newmenu;
    public static LatLng MyLocation;
    GpsTracker gps;
    Geocoder geocoder;
    public static double lat, lng;
    List<Address> addresses;
    public static String city, state, knownName, country,first_name,last_name,email,profile;
    boolean isNetwork;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        init();
        showHashKey(MainActivity.this);
        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //Get Current Location
        if (!isNetwork) {
            Utils.conDialog(MainActivity.this);
        } else {
            new getlocation().execute();
        }

        click_method();

    }


    private void init() {
        callbackManager = CallbackManager.Factory.create();
        isNetwork = Utils.isNetworkConnected(MainActivity.this);
        gps = new GpsTracker(MainActivity.this);
        geocoder = new Geocoder(this, Locale.getDefault());
        ll_login = (LinearLayout) findViewById(R.id.ll_login);
        ll_signup = (LinearLayout) findViewById(R.id.ll_signup);
        ll_facebook = (LinearLayout) findViewById(R.id.ll_facebook);
    }

    private void click_method() {
        ll_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
            }
        });

        ll_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,SignupActivity.class));
            }
        });

        ll_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginToFacebook();
                //startActivity(new Intent(MainActivity.this,SignupActivity.class));
            }
        });
    }

    //*************************************Location Async Task Classes******************************
    class getlocation extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //***********************************Get Current Location*******************************
            if (gps.canGetLocation()) {

                lat = gps.getLatitude();
                lng = gps.getLongitude();
                MyLocation = new LatLng(lat, lng);
                try {
                    addresses = geocoder.getFromLocation(lat, lng, 1);

                    String address = addresses.get(0).getAddressLine(0);
                    city = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getSubLocality();
                    knownName = addresses.get(0).getFeatureName();
                    Log.i("City: ", "" + city);

                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(ll_login, "Unable to get location",
                                    Snackbar.LENGTH_SHORT).show();
                        }
                    });

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (gps.canGetLocation()) {

            } else {
                Snackbar.make(ll_login, "Unable to get location",
                        Snackbar.LENGTH_SHORT).show();
            }
        }
    }


    // *********************************************Facebook Login**********************************
    public void loginToFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        RequestData();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        exception.printStackTrace();
                    }
                });
    }

    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                Log.i("JSON: ", "" + json);
                try {
                    if (json != null) {
                        String text = "<b>Name :</b> " + json.getString("name") + "<br><br><b>Email :</b> " + json.optString("email") + "<br><br><b>Profile link :</b> " + json.getString("link");
                        Log.i("Response", "data: " + text);
                        JSONObject jobj = json.getJSONObject("picture");
                        JSONObject data = jobj.getJSONObject("data");
                        String url = data.getString("url");
                        Log.i("Image Response", "" + url);
                        first_name = json.getString("name");
//                        String arr[] = first_name.split(" ");
//                        first_name = arr[0];
//                        last_name = arr[1];
                        Log.i("Name", "data: " + first_name + "" + last_name);
                        profile = url;
                        // Utils.image_url = profile;
                        email = json.optString("email");
                        Intent i = new Intent(MainActivity.this, SignupActivity.class);
                        i.putExtra("name", first_name);
                        i.putExtra("email", email);
                        ActivityStack.activity.add(MainActivity.this);
                        startActivity(i);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,picture.type(large),email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    //*************************************onActivityResult*****************************************
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // **********************************************Show Hashkey***********************************
    private void showHashKey(MainActivity mainActivity) {

        try {
            PackageInfo info = getPackageManager().getPackageInfo("kindness.app.kindnessnetwork", PackageManager.GET_SIGNATURES); // Your
            // here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("KeyHash:: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }
}
